
// OpenCV_Test_01Dlg.h: 헤더 파일
//
using namespace std;
using namespace cv; 



#pragma once


class COpenCVTest01Dlg : public CDialogEx
{
// 생성입니다.
public:
	COpenCVTest01Dlg(CWnd* pParent = nullptr);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_OPENCV_TEST_01_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;
	
	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	//extern "C" __declspec(dllexport)
	void COpenCVTest01Dlg::DisplayImage(int IDC_PICTURE_TARGET, Mat targetMat);
	//extern "C" __declspec(dllexport)

	void COpenCVTest01Dlg::DisplayImage(int IDC_PICTURE_TARGET, IplImage *targetMat);
	afx_msg void OnBnClickedButton2();

	bool bAction1;
	afx_msg void OnClose();
};
extern COpenCVTest01Dlg test;


